package be.yildizgames.test;

import be.yildizgames.module.graphic.GraphicEngine;
import be.yildizgames.module.graphic.GraphicWorld;
import be.yildizgames.common.geometry.Point3D;
import be.yildizgames.common.shape.Sphere;
import be.yildizgames.module.graphic.material.MaterialManager;
import be.yildizgames.module.graphic.material.Material;
import be.yildizgames.module.color.Color;

public class EntryPoint {

    public static void main(String[] args) {
        GraphicEngine engine = GraphicEngine.getEngine();
        GraphicWorld w = engine.createWorld();
        System.out.println(engine.getScreenSize());
        MaterialManager mm = new MaterialManager(engine);
        Material m = mm.createMaterial(Color.rgb(255,255,255));
        w.getDefaultCamera().setPosition(500,500,500);
        w.getDefaultCamera().lookAt(Point3D.ZERO);
        w.createMovableDoodad(Sphere.fromRadius(10), m);
        w.createPointLight("l1", Point3D.valueOfY(50));
        while(true) {
            engine.update();
        }
            
        
    }
}
